# Agri-Environmental Potential

This repository contains the code required to replicate the analyses as published in "Archetypes of agri-environmental potential: a multi-scale typology for spatial stratification and upscaling in Europe" by Michael Beckmann, Gregor Didenko, James M. Bullock, Anna F. Cord, Anne Paulus, Guy Ziv and Tomáš Václavík.

